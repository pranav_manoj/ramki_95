from django import forms
from .models import TVSchedule


class TVScheduleForm(forms.ModelForm):
    class Meta:
        model = TVSchedule
        fields = ['start_time', 'end_time']
