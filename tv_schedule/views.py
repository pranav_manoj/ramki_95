from django.shortcuts import render
from .models import TVSchedule


def turn_on_tv(request):
    tv_schedule = TVSchedule.objects.get(request)
    tv_schedule.status = True
    tv_schedule.save()
    return render(request, 'tv_schedule/list.html')


def turn_off_tv(request):
    tv_schedule = TVSchedule.objects.get(request)
    tv_schedule.status = False
    tv_schedule.save()
    return render(request, 'tv_schedule/list.html')
