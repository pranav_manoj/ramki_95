from django.urls import path
from . import views

app_name = 'tv_schedule'

urlpatterns = [
    path('turn-on/', views.turn_on_tv, name='turn_on_tv'),
    path('turn-off/', views.turn_off_tv, name='turn_off_tv'),
]
