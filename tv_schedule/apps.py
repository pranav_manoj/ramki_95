from django.apps import AppConfig


class TvScheduleConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'tv_schedule'
