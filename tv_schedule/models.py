from django.db import models


class TVSchedule(models.Model):
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    status = models.BooleanField(default=False)
